A simple mobile app using HTML, LESS and CSS.

#Installation
- Clone this repository to your project root
- Dependencies
  http-server
- npm install
- To run the project http-server
- Hit localhost:8080 in your browser.
